/**
 * @file simplecurlpp.h
 * @author joonatoona
 * @date 4 Mar 2018
 * @brief Main header for SimpleCurlPP
 */

#ifndef SIMPLECURLPP_H
#define SIMPLECURLPP_H

#include <iostream>
#include <string>
#include <unordered_map>
#include <unordered_set>

namespace SimpleCurlPP {

/** Type of HTTP Request to perform */
enum RequestType { GET, POST };
/** Type of HTTP Post body */
enum BodyType { URLENCODED, JSON };

const std::unordered_map<BodyType, std::string> BODY_TYPE_HEADER = {
    {BodyType::URLENCODED, "application/x-www-urlencoded"}, {BodyType::JSON, "application/json"}};

class NetworkResponse {
  public:
    long status; ///< HTTP status code

    std::string url; ///< Final URL For request

    std::string body;                                     ///< Response body
    std::unordered_map<std::string, std::string> headers; ///< Response headers

    std::string error; ///< libCURL error code
};

/**
 * @brief Base class for HTTP requests
 *
 * Executing a GET request:
 * \snippet example.cpp Executing a GET request
 *
 * Executing a POST request:
 * \snippet example.cpp Executing a POST request
 */
class NetworkRequest {
  public:
    /**
     * @param url URL for request
     * @param type Type of HTTP request
     */
    NetworkRequest(const std::string &url, const RequestType &type = RequestType::GET);
    NetworkRequest(const NetworkRequest &) = delete; // non-copyable (for now, to be safe)
    ~NetworkRequest();

    /**
     * Set request body (Only applicable to POST)
     * @param body Body to POST
     * @param bodyType Type of HTTP body
     */
    void setBody(const std::string &body, const BodyType &bodyType = BodyType::URLENCODED);

    void setHeader(const std::string &header, const std::string &value);
    void unsetHeader(const std::string &header);

    /**
     * Execute current request
     * @return HTTP Response
     */
    NetworkResponse send();

  private:
    void *m_curl;
    std::unordered_map<std::string, std::string> m_headers;
    std::unordered_set<std::string> m_rmHeaders;
    std::string m_url;
    RequestType m_type;
    std::string m_body;
};

NetworkResponse get(const std::string &url);

std::ostream &operator<<(std::ostream &os, const SimpleCurlPP::NetworkResponse &rs);

} // namespace SimpleCurlPP

#endif // SIMPLECURLPP_H

/** \example example.cpp
 * Example for using simplecurlpp to execute GET and POST requests
 */
