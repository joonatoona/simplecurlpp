add_executable(SCPPTest
    http.cpp
)

target_link_libraries(SCPPTest
    simplecurlpp
)
set_target_properties(SCPPTest PROPERTIES EXCLUDE_FROM_ALL TRUE)
set_target_properties(SCPPTest PROPERTIES
    RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}
)

add_custom_target(check
    COMMAND SCPPTest
    VERBATIM
    USES_TERMINAL
)
